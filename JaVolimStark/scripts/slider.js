const menuButton = document.querySelector('.menu')
const slideMenu = document.querySelector('.slide-menu')

function toggleClass(el, newClassName) {
    el.className.indexOf(newClassName) !== -1 ?
        el.className = el.className.replace(` ${newClassName}`, '') :
        el.className += ` ${newClassName}`
}


function toggleMenu() {
    let num = !slideMenu.style.width || slideMenu.style.width === '0%' ?
        slideMenu.style.width = '35vw' :
        slideMenu.style.width = '0%'
    return num
}

menuButton.onclick = function () {

    toggleClass(this, 'open')
    toggleMenu()
    $('.nav-link').toggle()
}

                