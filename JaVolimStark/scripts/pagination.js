// paginationContainer: contains HTML elements that need to be distributet by pages
// pageButtonsContainer: empty div that gets populated by page numbers
// nextPageControl: next page button
// previousPageControl: previous page button
// pagination: container for pagination controls

//visibleItemsCount: maximum number of items to show per page

//  INITIALIZE visibleItemsCount BEFORE INCLUDING THIS SCRIPT 
// Example:

    //<script>
    //    var visibleItemsCount = 4;
    //</script>
    //<script src="~/Scripts/pagination.js"></script>

// --------------------------------------------------------------------------------

var currentPage = 1;
var numberOfPages = 0;
var _visibleItemsCount = 3;

$(document).ready(function () {

    if (typeof visibleItemsCount !== 'undefined') {
        _visibleItemsCount = visibleItemsCount;
    }

    var paginationContainer = $("#paginationContainer")[0];
    if (paginationContainer.children) {
        numberOfPages = Math.ceil(getVisibleItems(paginationContainer).length / _visibleItemsCount);
        if (numberOfPages > 1) {
            for (var i = 1; i <= numberOfPages; i++) {
                $("#pageButtonsContainer").append("<span id='page" + i.toString() + "Control' data-pagenumber='" + i.toString() + "'>" + i.toString() + "</span>")
                initPageControl(i);
            }
        }
        else {
            $("#pagination").addClass("hidden");
        }
    }

    $("#nextPageControl").on('click', event => {
        nextPage();
    });

    $("#previousPageControl").on('click', event => {
        previousPage();
    });
});

function getVisibleItems(paginationContainer) {
    var items = [];
    for (var i = 0; i < paginationContainer.children.length; i++) {
        var item = paginationContainer.children[i];
        if (!item.classList.contains("placeholder")) {
            items.push(item);
        }
    }
    return items;
}

function initPageControl(pageNumber) {
    var pageControlId = "#page" + pageNumber.toString() + "Control";
    if (pageNumber == 1) {
        if (!$(pageControlId).hasClass("active")) {
            $(pageControlId).addClass("active");
        }
    }
    $(pageControlId).on('click', event => {
        const clickedPageControl = $(event.target)[0];
        if (clickedPageControl) {
            if (clickedPageControl.dataset.pagenumber) {
                var clickedPageNumber = parseInt(clickedPageControl.dataset.pagenumber);
                goToPage(clickedPageNumber);
            }
        }
    });
}

function goToPage(pageNumber) {
    var paginationContainer = $("#paginationContainer")[0];
    if (paginationContainer.children) {
        for (var i = 0; i < paginationContainer.children.length; i++) {
            var item = paginationContainer.children[i];
            if (i > (pageNumber - 1) * _visibleItemsCount && i <= pageNumber * _visibleItemsCount) {
                if (item.classList.contains("hidden")) {
                    item.classList.remove("hidden");
                }
            }
            else {
                if (!item.classList.contains("hidden")) {
                    item.classList.add("hidden");
                }
            }
        }
        setActivePageControl(pageNumber);
        currentPage = pageNumber;
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#paginationContainer").offset().top
        }, 400, "swing");
    }
}

function setActivePageControl(pageNumber) {
    var pageButtonsContainer = $("#pageButtonsContainer")[0];
    if (pageButtonsContainer) {
        if (pageButtonsContainer.children) {
            for (var i = 0; i < pageButtonsContainer.children.length; i++) {
                var pageControl = pageButtonsContainer.children[i];
                if (pageControl.dataset.pagenumber == pageNumber) {
                    if (!pageControl.classList.contains("active")) {
                        pageControl.classList.add("active");
                    }
                }
                else {
                    if (pageControl.classList.contains("active")) {
                        pageControl.classList.remove("active");
                    }
                }
            }
        }
    }
}

function nextPage() {
    if (numberOfPages >= currentPage + 1) {
        goToPage(currentPage + 1);
    }

}

function previousPage() {
    if (currentPage > 1) {
        goToPage(currentPage - 1);
    }
}
                