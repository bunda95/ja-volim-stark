var searchInputOpen = false;

// --------------------- SEARCH ---------------------
$("#search-button").click(function () {
    if (!searchInputOpen) {
        toggleSearchInput()
    }
    else {
        if ($("#search-input").val() != "" && $("#search-input").val() != null) {
            executeSearch();
        }
        else {
            toggleSearchInput()
        }
    }
});

$("#search-input").keypress(function (event) {
    if (event.keyCode == 13) {
        executeSearch();
    }
});

function toggleSearchInput() {
    $('#search-input').toggleClass('active');
    $("#search-input").focus();
    searchInputOpen = !searchInputOpen;
}

function executeSearch() {
    var searchText = $("#search-input").val();
    var searchLink = $("#search-input")[0].dataset.link;

    window.location = searchLink + '?search=' + searchText;
}
                