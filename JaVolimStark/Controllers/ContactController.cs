﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Net.Mail;
using System.Text;
using JaVolimStark.Models;

namespace JaVolimStark.Controllers
{
    public class ContactController : SurfaceController
    {
        #region Http Actions

        [HttpPost]
        public ActionResult Contact(ContactModel model)
        {
            if (ModelState.IsValid)
            {
                var sendEmailsFrom = System.Configuration.ConfigurationManager.AppSettings["ContactEmailFrom"];
                var sendEmailsTo = System.Configuration.ConfigurationManager.AppSettings["ContactEmailTo"];
                var subject = System.Configuration.ConfigurationManager.AppSettings["ContactEmailSubject"];

                var body = new StringBuilder();
                body.AppendFormat("<p>Ime i prezime: {0}</p>", model.FullName);
                body.AppendFormat("<p>E-mail: {0}</p>", model.Email);
                body.AppendFormat("<p>Poruka: {0}</p>", model.Message);

                bool succesfullySentMail = SendMail(subject, sendEmailsFrom, sendEmailsTo, model.Email, body);

                if (succesfullySentMail)
                {
                    // Clear all the form fields
                    ModelState.Clear();
                    model.FullName = string.Empty;
                    model.Email = string.Empty;
                    model.Message = string.Empty;

                    //redirect to current page to clear the form
                    return RedirectToCurrentUmbracoPage();
                }
            }

            return CurrentUmbracoPage();
        }

        #endregion

        #region Helper methods

        private bool SendMail(string subject, string defaultEmailFrom, string defaultEmailTo, string modelEmail, StringBuilder body)
        {
            bool result = false;

            SmtpClient smtp = new SmtpClient();
            MailMessage message = new MailMessage();

            message.To.Add(defaultEmailTo);

            if (string.IsNullOrEmpty(modelEmail))
            {
                message.From = new MailAddress(defaultEmailFrom);
                message.Sender = new MailAddress(defaultEmailFrom);
            }
            else
            {
                message.From = new MailAddress(modelEmail);
                message.Sender = new MailAddress(modelEmail);
            }

            message.Body = body.ToString();
            message.IsBodyHtml = true;
            message.Subject = subject;

            try
            {
                smtp.Send(message);
                TempData["SuccesfullySentMail"] = true;
                result = true;
            }
            catch (Exception ex)
            {
                TempData["SuccesfullySentMail"] = false;
            }

            return result;
        }

        #endregion
    }
}