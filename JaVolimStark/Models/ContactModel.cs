﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace JaVolimStark.Models
{
    public class ContactModel
    {
        [Required(ErrorMessage = "Obavezno polje")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Obavezno polje")]
        [RegularExpression("^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}$", ErrorMessage = "Krivi format emaila")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Obavezno polje")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Obavezno polje")]
        public string Phone { get; set; }

        public string Company { get; set; }

        [Required(ErrorMessage = "Obavezno polje")]
        public string Message { get; set; }
    }
}