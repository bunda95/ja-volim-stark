﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JaVolimStark.Models
{
    public class NewsletterModel
    {
        [Required(ErrorMessage = "Obavezno polje")]
        [RegularExpression("^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}$", ErrorMessage = "Krivi format emaila")]
        public string Email { get; set; }
    }
}